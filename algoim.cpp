#include <iostream>
#include <iomanip>
#include <fstream>
#include "../algoim/algoim/quadrature_multipoly.hpp"
//#include "../algoim/algoim/quadrature_general.hpp"
#include "algoim.h"

using namespace algoim;

/* 
  First iteration at a basic C wrapper with algoim.
  This will eventually evolve into a full PETSc wrapper
  with arbitrary geometries and not just 3D and planar lens
  geometries.
*/

template<int N>
struct Ball
{
    template<typename T>
    T operator() (const algoim::uvector<T,N>& x) const
    {
        return x(0)*x(0) + x(1)*x(1) + x(2)*x(2);
    }

    template<typename T>
    algoim::uvector<T,N> grad(const algoim::uvector<T,N>& x) const
    {
        return algoim::uvector<T,N>(2.0*x(0), 2.0*x(1), 2.0*x(2));
    }
};


template<int N>
void outputQuadratureRuleAsVtpXML(const std::vector<uvector<real,N+1>>& q, std::string fn)
{
    static_assert(N == 2 || N == 3, "outputQuadratureRuleAsVtpXML only supports 2D and 3D quadrature schemes");
    std::ofstream stream(fn);
    stream << "<?xml version=\"1.0\"?>\n";
    stream << "<VTKFile type=\"PolyData\" version=\"0.1\" byte_order=\"LittleEndian\">\n";
    stream << "<PolyData>\n";
    stream << "<Piece NumberOfPoints=\"" << q.size() << "\" NumberOfVerts=\"" << q.size() << "\" NumberOfLines=\"0\" NumberOfStrips=\"0\" NumberOfPolys=\"0\">\n";
    stream << "<Points>\n";
    stream << "  <DataArray type=\"Float32\" Name=\"Points\" NumberOfComponents=\"3\" format=\"ascii\">";
    for (const auto& pt : q)
        stream << pt(0) << ' ' << pt(1) << ' ' << (N == 3 ? pt(2) : 0.0) << ' ';
    stream << "</DataArray>\n";
    stream << "</Points>\n";
    stream << "<Verts>\n";
    stream << "  <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">";
    for (size_t i = 0; i < q.size(); ++i)
        stream << i << ' ';
    stream <<  "</DataArray>\n";
    stream << "  <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">";
    for (size_t i = 1; i <= q.size(); ++i)
        stream << i << ' ';
    stream << "</DataArray>\n";
    stream << "</Verts>\n";
    stream << "<PointData Scalars=\"w\">\n";
    stream << "  <DataArray type=\"Float32\" Name=\"w\" NumberOfComponents=\"1\" format=\"ascii\">";
    for (const auto& pt : q)
        stream << pt(N) << ' ';
    stream << "</DataArray>\n";
    stream << "</PointData>\n";
    stream << "</Piece>\n";
    stream << "</PolyData>\n";
    stream << "</VTKFile>\n";
};

// Driver method which takes a functor phi defining a single polynomial in the reference
// rectangle [xmin, xmax]^N, of Bernstein degree P, builds a quadrature scheme with the
// given q, and outputs it for visualisation in a set of VTP XML files
template<int N, typename F>
void outputQuadScheme(const F& fphi, real xmin, real xmax, const uvector<int,N>& P, int q, std::string qfile, double** points, double** weights, int* ncp)
{
    // Construct phi by mapping [0,1] onto bounding box [xmin,xmax]
    xarray<real,N> phi(nullptr, P);
    algoim_spark_alloc(real, phi);
    bernstein::bernsteinInterpolate<N>([&](const uvector<real,N>& x) { return fphi(xmin + x * (xmax - xmin)); }, phi);

    // Build quadrature hierarchy
    ImplicitPolyQuadrature<N> ipquad(phi);

    // Compute quadrature scheme and record the nodes & weights; phase0 corresponds to
    // {phi < 0}, phase1 corresponds to {phi > 0}, and surf corresponds to {phi == 0}.
    std::vector<uvector<real,N+1>> phase0, phase1, surf;
    ipquad.integrate(AutoMixed, q, [&](const uvector<real,N>& x, real w)
    {
        if (bernstein::evalBernsteinPoly(phi, x) < 0)
            phase0.push_back(add_component(x, N, w));
    });
#if 0
    ipquad.integrate_surf(AutoMixed, q, [&](const uvector<real,N>& x, real w, const uvector<real,N>& wn)
    {
        surf.push_back(add_component(x, N, w));
    });
#endif 
    // output to file
    //outputQuadratureRuleAsVtpXML<N>(phase0, qfile + "-phase0.vtp");
    //outputQuadratureRuleAsVtpXML<N>(phase1, qfile + "-phase1.vtp");
    //outputQuadratureRuleAsVtpXML<N>(surf, qfile + "-surf.vtp");

    int np;
    np = phase0.size();
    switch(N){
    case 2:
      *ncp = np;
      *points  = (double*)calloc(np*2, sizeof(double));
      *weights = (double*)calloc(np, sizeof(double));
      for (int i = 0; i < np; ++i){
        (*points)[i*2+0] = phase0[i](0);
        (*points)[i*2+1] = phase0[i](1);
        (*weights)[i] = phase0[i](2);
      }
      break;
    case 3:
      *ncp = np;
      *points  = (double*)calloc(np*3, sizeof(double));
      *weights = (double*)calloc(np, sizeof(double));
      for (int i = 0; i < np; ++i){
        (*points)[i*3+0] = phase0[i](0);
        (*points)[i*3+1] = phase0[i](1);
        (*points)[i*3+2] = phase0[i](2);
        (*weights)[i] = phase0[i](3);
      }
      break;
    default:
      printf("No support for this dimension.\n");
      break;
    }
}

void sphereTest2D(double rad, int order, int npls, double** points, double** weights, int* ncp)
{
        auto phi0 = [rad](const uvector<real,2>& xx)
        {
            real x = xx(0);
            real y = xx(1);
            return x*x + y*y - rad;
        };
        
        outputQuadScheme<2>(phi0, -rad - rad/10., rad + rad/10., order, npls, "oneDisc", points, weights, ncp);
}

void sphereTest3D(double rad, int order, int npls, double** points, double** weights, int* ncp)
{
        auto phi0 = [rad](const uvector<real,3>& xx)
        {
            real x = xx(0);
            real y = xx(1);
            real z = xx(2);
            return x*x + y*y + z*z - 1.;
        };

        outputQuadScheme<3>(phi0, -1.1, 1.1, order, npls, "oneSphere", points, weights, ncp);
}

extern "C"{
  /* Generate a quadrature on a sphere of radius 1, convert points back into real space */
  int GenerateSphericalQuadrature(int dim, double rad, int order, int npls, double** points, double** weights, int* ncp){
 
    if (dim == 2)sphereTest2D(rad, order, npls, points, weights, ncp);
    else sphereTest3D(rad, order, npls, points, weights, ncp);
    for (int i = 0; i < *ncp; ++i){
      for (int d = 0; d < dim; ++d) (*points)[i*dim+d] *= (rad);
      for (int d = 0; d < dim; ++d) (*points)[i*dim+d] -= (rad)/2.;
      (*weights)[i] *= pow(rad, dim); // this is normalizing the area to unity
    }
    return 0;
  }


}