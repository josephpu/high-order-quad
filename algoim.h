#ifndef __ALGOIM_H
#define __ALGOIM_H

#ifdef __cplusplus

void test3d(int n, double rad, double distance);
extern "C" {
#endif
/*
  First iteration for basic C wrapper. Later iterations will properly
  encapsulate the full functionality of algoim for use with PETSc.

  C++ API for PETSc. This function will interface with the Algoim functions
  to generate a quadrature on the lens formed by overlapping spheres. 

  Input: Distance between spherical centers.
  Output: An array of quadrature points with an array of weights.

*/
int GenerateQuadrature(int dim, double rad, double distance, double** points, double** weights, int* ncp);
int GenerateSphericalQuadrature(int dim, double rad, int order, int npls, double** points, double** weights, int* ncp);

#ifdef __cplusplus
}
#endif
#endif