static char help[] = "Particle Basis Landau Example using nonlinear solve + Implicit Midpoint-like time stepping.";

/*
  ${PETSC_DIR}/src/ts/tests/ex27.c from PETSc. Implements the particle basis landau collision
  operator from [1] with the fully conservative time stepping scheme of [2]. Quadrature on the
  overlapping particle regions defined by \Gamma(S_\epsilon^n, p, \bar p) in equation 42 of [2]
  are formed from algoim presented in [3] in the 3V case. Planar lens not yet implemented with 2V
  being carried out as an approximation to the integral via dropping dimensions and performing
  a 1D integral over the regions with thermal convergence demonstrated in [4], but planar lenses
  are expected to improve the behavior of this operator with truncation at the support of machine \epsilon 
*/

/* References
  [1] https://arxiv.org/abs/1910.03080v2
  [2] https://arxiv.org/pdf/2012.07187.pdf
  [3] https://www.sciencedirect.com/science/article/pii/S002199912100615X?via%3Dihub
  [4] Pusztay, Joseph, and Matthew Knepley. “A Particle Basis Vlasov-Poisson-Landau Solver for Plasma Simulation in PETSc.” 
      ProQuest Dissertations and Theses, State University of New York at Buffalo, 2022, p. 108. 
*/

#include <petscdmplex.h>
#include <petscdmswarm.h>
#include <petscts.h>
#include <petscviewer.h>
#include <petscmath.h>
#include <petsclandau.h>
#include "algoim.h"

#define BOLTZMANN_K 1.380649e-23 /* J/K */
#define KEV_J 6.241506479963235e15 /*  */
#define LIGHT_C 299792458
#define EPSILON_NOUGHT 8.8542e-12
#define ELEMENTARY_CHARGE 1.602176e-19
#define ELECTRON_MASS 9.10938356e-31
#define PROTON_MASS 1.6726219e-27

#define LANDAU_MAX_SPECIES 10

typedef struct {
  /* Velocity space grid and functions */
  PetscInt  steps;                          /* Number of time steps */
  PetscReal step_size;                      /* Size of the time step */
  PetscReal gaussian_w;                     /* Width of quadrature evaulation on gaussian mollifiers */
  PetscReal epsilon;                        /* Gaussian regularization parameter */
  PetscReal t_0;                            /* time nondimensionalization */
  PetscInt  mass_units[LANDAU_MAX_SPECIES]; /* 0 for electron mass units, 1 for proton mass units */
  PetscReal masses[LANDAU_MAX_SPECIES];     /* Electron, Sr+ Mass [kg] */
  PetscReal T[LANDAU_MAX_SPECIES];          /* Electron, Ion Temperature [K] */
  PetscReal v0[LANDAU_MAX_SPECIES];         /* Species mean velocity in 1D */
  PetscReal n0[LANDAU_MAX_SPECIES];
  PetscReal charges[LANDAU_MAX_SPECIES];
  PetscBool anisotropic; 
} AppCtx;

static PetscErrorCode ProcessOptions(MPI_Comm comm, AppCtx *options)
{
  PetscInt   nn0=LANDAU_MAX_SPECIES, nT=LANDAU_MAX_SPECIES;
  PetscInt   nmu=LANDAU_MAX_SPECIES, nm=LANDAU_MAX_SPECIES;
  PetscInt   nc=LANDAU_MAX_SPECIES;
  PetscBool  nmuflg, Tflg, cflg;
  
  PetscFunctionBeginUser;
  options->anisotropic   = PETSC_FALSE;
  options->gaussian_w    = -1.;
  options->step_size     = 0.1;
  options->steps         = 1;
  options->epsilon       = 1.9;
  options->T[0]          = 5*1.1604525e7; /* 5kev converted to kelvin */
  options->T[1]          = 5*1.1604525e7; /* 5kev converted to kelvin */
  options->masses[0]     = ELECTRON_MASS;
  options->masses[1]     = 2*PROTON_MASS;
  options->mass_units[0] = 0;
  options->mass_units[1] = 0;
  options->charges[0]    = -ELEMENTARY_CHARGE;
  options->charges[1]    = ELEMENTARY_CHARGE;
  options->n0[0]         = 1.0e20;
  options->n0[1]         = 1.0e20;

  PetscOptionsBegin(comm, "", "Collision Options", "DMPLEX");
  PetscCall(PetscOptionsBool("-anisotropic", "test anisotropic maxwellians", "ex29.c", options->anisotropic, &options->anisotropic, NULL));
  PetscCall(PetscOptionsInt("-steps", "max number of time steps to take", "ex29.c", options->steps, &options->steps, NULL));
  PetscCall(PetscOptionsReal("-step_size", "size of the time step", "ex29.c", options->step_size, &options->step_size, NULL));
  PetscCall(PetscOptionsReal("-gaussian_width", "Width of entropy gradient quadrature evaluation", "ex29.c", options->gaussian_w, &options->gaussian_w, NULL));
  PetscCall(PetscOptionsReal("-epsilon", "Mollification parameter", "ex29.c", options->epsilon, &options->epsilon, NULL));
  PetscCall(PetscOptionsRealArray("-dm_swarm_number_density", "The non normalized number density of each species", "", options->n0, &nn0, NULL));
  PetscCall(PetscOptionsRealArray("-dm_swarm_temperature", "The temperature of each species in KeV", "", options->T, &nT, &Tflg));
  PetscCall(PetscOptionsIntArray("-dm_swarm_mass_units", "Fundamental mass to use for each species", "", options->mass_units, &nmu, &nmuflg));
  PetscCall(PetscOptionsRealArray("-dm_swarm_masses", "The mass of each species in multiples of fundamental mass units", "", options->masses, &nm, NULL));
  PetscCall(PetscOptionsRealArray("-dm_swarm_charges", "The charge of each species in fundamental charge units (-1,2,3...)", "", options->charges, &nc, &cflg));
  PetscOptionsEnd();
  
  /* If mass units were specified, get the mass array and compute masses */
  if(nmuflg) {
    if (nm != nmu) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_ARG_OUTOFRANGE, "Number of mass units and masses set not equal");
    PetscInt idx;
    for (idx = 0; idx < nmu; ++idx) options->masses[idx] = options->mass_units[idx] == 0 ? ELECTRON_MASS*options->masses[idx] : PROTON_MASS*options->masses[idx];
  }
  if(Tflg) {
    PetscInt idx;
    if (options->anisotropic) {
      /* For now just hardcode the y and z directions to be 2KeV lower than x */
      PetscInt dim = 3;

      PetscPrintf(PETSC_COMM_WORLD, "Anisotropic setup.\n");
      // Should put in check that T array matches the dimension
      // this is wrong and putting things in nutty places
      for (idx = 0; idx < nc; ++idx) {
        for (PetscInt d = 0; d < dim; ++d) options->T[idx*dim + d] *= 1.1604525e7;
      }
      for (idx = 0; idx < nc; ++idx) {
        for (PetscInt d = 0; d < dim; ++d) options->v0[idx*dim + d] = PetscSqrtReal(BOLTZMANN_K * options->T[idx*dim+d] / options->masses[idx]);
        for (PetscInt d = 0; d < dim; ++d) PetscPrintf(PETSC_COMM_WORLD, "v[%i] for species %i: %g\n", d, idx, options->v0[idx*dim + d]);
      }
    
    }
    else{
      for (idx = 0; idx < nT; ++idx) options->T[idx] *= 1.1604525e7;
      for (idx = 0; idx < nT; ++idx) options->v0[idx] = PetscSqrtReal(BOLTZMANN_K * options->T[idx] / options->masses[idx]);
    }
  }
  if(cflg){
    PetscInt idx;
    for (idx = 0; idx < nc; ++idx) options->charges[idx] *= ELEMENTARY_CHARGE;
  }

  PetscFunctionReturn(0);
}

static PetscErrorCode CreateMesh(MPI_Comm comm, DM *dm, AppCtx *user)
{
  PetscFunctionBeginUser;
  PetscCall(DMCreate(comm, dm));
  PetscCall(DMSetType(*dm, DMPLEX));
  PetscCall(DMSetFromOptions(*dm));
  PetscCall(DMViewFromOptions(*dm, NULL, "-dm_view"));
  PetscFunctionReturn(0);
}

/*
  This function to be reduced to the velocity function passed to swarm.
  I don't feel like recompiling the library every time i need to check
  something in it right now though.

  v0 is now of size Ns*dim
*/
PetscErrorCode DMSwarmInitializeVelocities_Anisotropic(DM sw, PetscProbFunc sampler, const PetscReal v0[])
{
  PetscReal           *v;
  PetscInt            *species;
  void                *ctx;
  PetscInt             dim, Np, p;

  PetscFunctionBegin;

  PetscCall(DMGetDimension(sw, &dim));
  PetscCall(DMSwarmGetLocalSize(sw, &Np));
  PetscCall(DMSwarmGetField(sw, "velocity", NULL, NULL, (void **) &v));
  PetscCall(DMSwarmGetField(sw, "species", NULL, NULL, (void **) &species));
  if (v0[0] == 0.) PetscCall(PetscArrayzero(v, Np*dim));
  else {
    PetscRandom  rnd;

    PetscCall(PetscRandomCreate(PetscObjectComm((PetscObject) sw), &rnd));
    PetscCall(PetscRandomSetInterval(rnd, 0, 1.));
    PetscCall(PetscRandomSetFromOptions(rnd));

    for (p = 0; p < Np; ++p) {
      PetscInt  s = species[p], d;
      PetscReal a[3], vel[3];

      for (d = 0; d < dim; ++d) PetscCall(PetscRandomGetValueReal(rnd, &a[d]));
      PetscCall(sampler(a, NULL, vel));
      for (d = 0; d < dim; ++d) {v[p*dim+d] = (v0[s*dim+d] / (v0[0])) * vel[d];}
    }
    PetscCall(PetscRandomDestroy(&rnd));
  }
  PetscCall(DMSwarmRestoreField(sw, "velocity", NULL, NULL, (void **) &v));
  PetscCall(DMSwarmRestoreField(sw, "species", NULL, NULL, (void **) &species));
  PetscFunctionReturn(0);
}

static PetscErrorCode CreateSwarm(DM dm, AppCtx *user, DM *sw)
{
  PetscInt       dim;

  PetscFunctionBeginUser;
  PetscCall(DMGetDimension(dm, &dim));
  PetscCall(DMCreate(PetscObjectComm((PetscObject) dm), sw));
  PetscCall(DMSetType(*sw, DMSWARM));
  PetscCall(DMSetDimension(*sw, dim));
  PetscCall(DMSwarmSetType(*sw, DMSWARM_PIC));
  PetscCall(DMSwarmSetCellDM(*sw, dm));
  PetscCall(DMSwarmRegisterPetscDatatypeField(*sw, "w_q", 1, PETSC_SCALAR));
  PetscCall(DMSwarmRegisterPetscDatatypeField(*sw, "velocity", dim, PETSC_REAL));
  PetscCall(DMSwarmRegisterPetscDatatypeField(*sw, "species", 1, PETSC_INT));
  PetscCall(DMSwarmRegisterPetscDatatypeField(*sw, "gradS", dim, PETSC_REAL));
  PetscCall(DMSwarmFinalizeFieldRegister(*sw));
  PetscCall(DMSwarmComputeLocalSizeFromOptions(*sw));
  PetscCall(DMSwarmInitializeCoordinates(*sw));
  if (user->anisotropic){
    PetscProbFunc  sampler;

    PetscCall(PetscProbCreateFromOptions(dim, "", "-dm_swarm_velocity_density", NULL, NULL, &sampler));
    
    PetscCall(DMSwarmInitializeVelocities_Anisotropic(*sw, sampler, user->v0));

  }
  else PetscCall(DMSwarmInitializeVelocitiesFromOptions(*sw, user->v0));
  /* Testing calls to register from command line */
  PetscCall(DMSetFromOptions(*sw));
  PetscCall(PetscObjectSetName((PetscObject) *sw, "Particles"));
  PetscCall(DMViewFromOptions(*sw, NULL, "-swarm_view"));
  PetscFunctionReturn(0);
}

/* Internal dmplex function, same as found in dmpleximpl.h */
static void DMPlex_WaxpyD_Internal(PetscInt dim, PetscReal a, const PetscReal *x, const PetscReal *y, PetscReal *w)
{
  PetscInt d;

  for (d = 0; d < dim; ++d) w[d] = a*x[d] + y[d];
}

/* Internal dmplex function, same as found in dmpleximpl.h */
static PetscReal DMPlex_DotD_Internal(PetscInt dim, const PetscScalar *x, const PetscReal *y)
{
  PetscReal sum = 0.0;
  PetscInt d;

  for (d = 0; d < dim; ++d) sum += PetscRealPart(x[d])*y[d];
  return sum;
}

/* Internal dmplex function, same as found in dmpleximpl.h */
static void DMPlex_MultAdd2DReal_Internal(const PetscReal A[], PetscInt ldx, const PetscScalar x[], PetscScalar y[])
{
  PetscScalar z[2];
  z[0] = x[0]; z[1] = x[ldx];
  y[0]   += A[0]*z[0] + A[1]*z[1];
  y[ldx] += A[2]*z[0] + A[3]*z[1];
  (void)PetscLogFlops(6.0);
}

/* Internal dmplex function, same as found in dmpleximpl.h to avoid private includes. */
static void DMPlex_MultAdd3DReal_Internal(const PetscReal A[], PetscInt ldx, const PetscScalar x[], PetscScalar y[])
{
  PetscScalar z[3];
  z[0] = x[0]; z[1] = x[ldx]; z[2] = x[ldx*2];
  y[0]     += A[0]*z[0] + A[1]*z[1] + A[2]*z[2];
  y[ldx]   += A[3]*z[0] + A[4]*z[1] + A[5]*z[2];
  y[ldx*2] += A[6]*z[0] + A[7]*z[1] + A[8]*z[2];
  (void)PetscLogFlops(15.0);
}

/*
  Gaussian - The Gaussian function G(x)

  Input Parameters:
+  dim   - The number of dimensions, or size of x
.  mu    - The mean, or center
.  sigma - The standard deviation, or width
-  x     - The evaluation point of the function

  Output Parameter:
. ret - The value G(x)
*/
static PetscReal Gaussian(PetscInt dim, const PetscReal mu[], PetscReal sigma, const PetscReal x[])
{
  PetscReal arg = 0.0;
  PetscInt  d;

  for (d = 0; d < dim; ++d) arg += PetscSqr(x[d] - mu[d]);
  return PetscPowReal(2.0*PETSC_PI*sigma, -dim/2.0) * PetscExpReal(-arg/(2.0*sigma));
}

static PetscErrorCode ComputeGradSExperimental(PetscInt pcdim, PetscReal* weight, PetscInt Np, const PetscReal vp[], const PetscReal velocity[], PetscReal integral[], PetscInt pidx, AppCtx *ctx)
{
  PetscReal sum = 0., epsilon=ctx->epsilon, vc_l[3];
  PetscInt  p;
  int d, dim = (int) pcdim;
  PetscFunctionBeginHot;
  for (d = 0; d < dim; ++d) integral[d] = 0.0;
  /* find necessary bounding box if one is not given */
  if (ctx->gaussian_w <= 0) {
    ctx->gaussian_w = PetscSqrtReal(-2*epsilon*PetscLogReal(1000.*PETSC_MACHINE_EPSILON/(2*epsilon)));
  }

  double *points, *qw;
  int ncp;
  /* 
    Generate a quadrature on the sphere surrounding the particle
    loop over quadrature points and evaluate at the particle indices.
    We can leverage two things here. Firstly, with this quadrature, 
    we are much faster than currently capable of just doing overlaps.
    We don't have to generate a quadrature over each pair of particles.
    We can leverage the asymmetry of the action and just compute on one
    quadrature in regards to one sphere. 
   */
  //PetscPrintf(PETSC_COMM_WORLD, "Quad gen\n\n");
  PetscCall(GenerateSphericalQuadrature((int) pcdim, ctx->gaussian_w, &points, &qw, &ncp));
  
  for(int i = 0; i < ncp; ++i){
    
    sum = 0.;
    for (p = 0; p < Np; ++p){
      if (p == pidx) continue;
      PetscReal distance[3];

      for (d = 0; d < dim; ++d) distance[d] = (vp[d] - velocity[p*dim+d]); 
      sum += weight[p]* qw[i] * Gaussian(dim, distance, epsilon, vc_l);
    }
    sum = PetscLogReal(sum);
    //PetscPrintf(PETSC_COMM_WORLD, "sum: %g qweight: %g\n", sum, qw[i]);
    for (d = 0; d < dim; ++d) integral[d] += qw[i]*(1./(epsilon))*(vp[d] - vc_l[d])*(Gaussian(dim, vp, epsilon, vc_l)) * (1. + sum);
  }
  free(points);
  free(qw);
  
  
  PetscFunctionReturn(0);
}


/* Q = 1/|xi| (I - xi xi^T / |xi|^2), xi = vp - vq */
static PetscErrorCode QCompute(PetscInt dim, const PetscReal vp[], const PetscReal vq[], PetscReal Q[])
{
  PetscReal xi[3], xi2, xi3, mag;
  PetscInt  d, e;

  PetscFunctionBeginHot;
  DMPlex_WaxpyD_Internal(dim, -1.0, vq, vp, xi);
  xi2 = DMPlex_DotD_Internal(dim, xi, xi);
  mag = PetscSqrtReal(xi2);
  xi3 = xi2 * mag;
  for (d = 0; d < dim; ++d) {
    for (e = 0; e < dim; ++e) {
      Q[d*dim+e] = -xi[d]*xi[e] / xi3;
    }
    Q[d*dim+d] += 1. / mag;
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode RHSFunctionParticles(TS ts, PetscReal t, Vec U, Vec R, void *ctx)
{
  AppCtx            *user = (AppCtx*)ctx;
  PetscInt           dbg  = 0;
  DM                 sw;                  /* Particles */
  const PetscScalar *u;                   /* input solution vector */
  PetscScalar       *r;
  PetscReal         **gradS;
  PetscReal          nu_alpha[LANDAU_MAX_SPECIES], nu_beta[LANDAU_MAX_SPECIES];
  PetscReal          lnLam=10., t0, nu_nd, m0=user->masses[0];
  PetscInt           dim, d, Np, p, q, s, *species, Ns;
 
  PetscFunctionBeginUser;

  PetscCall(TSGetDM(ts, &sw));
  PetscCall(DMSwarmGetNumSpecies(sw, &Ns));
  PetscCall(DMGetDimension(sw, &dim));
  /* Get the DMSwarm properties set at run time. */
  /* DMSwarmGetProperty(sw, "prop", *propArr) */
  /* Non dimensionalization of \nu, todo: nondimensionalization to be moved out of the solver into a part of swarm in future updates. */
  if (user->anisotropic) {
    t0 = 8*PETSC_PI*PetscSqr(EPSILON_NOUGHT*m0/PetscSqr(user->charges[0]*ELEMENTARY_CHARGE))/lnLam/user->n0[0]*PetscPowReal(user->v0[0],3);
    nu_nd = t0*user->n0[0]/PetscPowReal(user->v0[0],3);
  } else{
    t0 = 8*PETSC_PI*PetscSqr(EPSILON_NOUGHT*m0/PetscSqr(user->charges[0]*ELEMENTARY_CHARGE))/lnLam/user->n0[0]*PetscPowReal(user->v0[0],3);
    nu_nd = t0*user->n0[0]/PetscPowReal(user->v0[0],3);
  }
  /* \nu_\alpha\beta */
  for (s = 0; s < Ns; ++s){
    nu_alpha[s] = PetscSqr(user->charges[s]*ELEMENTARY_CHARGE/m0)*m0/user->masses[s];
    nu_beta[s] = PetscSqr(user->charges[s]*ELEMENTARY_CHARGE/EPSILON_NOUGHT)*lnLam / (8*PETSC_PI) * nu_nd;
  }
  PetscCall(VecZeroEntries(R));
  PetscCall(VecGetLocalSize(U, &Np));
  PetscCall(VecGetArray(R, &r));
  PetscCall(VecViewFromOptions(U, NULL, "-sol_view"));
  PetscCall(VecGetArrayRead(U, &u));
  Np  /= dim;
  /* The dmswarm stores dS/dv_p precomputed in pre step */
  PetscCall(DMSwarmGetField(sw, "gradS", NULL, NULL, (void **) &gradS));
  PetscCall(DMSwarmGetField(sw, "species", NULL, NULL, (void **) &species));

  if (dbg) {PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Part  ppr     x        y\n"));}
  for (p = 0; p < Np; ++p) {
    for (q = 0; q < Np; ++q) {
      PetscReal GammaS[3] = {0., 0., 0.}, Q[9];

      if (q == p) continue;
      DMPlex_WaxpyD_Internal(dim, -1.0, (const PetscReal*)&gradS[q*dim], (const PetscReal*)&gradS[p*dim], GammaS);
      /* Account for nu_\alpha\beta AND PARTICLE WEIGHT UNTIL ITS CORRECTED IN SWARM */
      for (d=0; d < dim; ++d) GammaS[d] *= nu_alpha[species[p]]*nu_beta[species[q]] * 1./Np;
      PetscCall(QCompute(dim, &u[p*dim], &u[q*dim], Q));
      switch (dim) {
        case 2: DMPlex_MultAdd2DReal_Internal(Q, 1, GammaS, &r[p*dim]);break;
        case 3: DMPlex_MultAdd3DReal_Internal(Q, 1, GammaS, &r[p*dim]);break;
        default: SETERRQ(PETSC_COMM_SELF, PETSC_ERR_ARG_OUTOFRANGE, "Do not support dimension%" PetscInt_FMT, dim);
      }
    }
    if (dbg) PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Final %4" PetscInt_FMT " %10.8lf %10.8lf\n", p, r[p*dim+0], r[p*dim+1]));
  }
  PetscCall(DMSwarmRestoreField(sw, "gradS", NULL, NULL, (void **) &gradS));
  PetscCall(DMSwarmRestoreField(sw, "species", NULL, NULL, (void **) &species));
  PetscCall(VecRestoreArrayRead(U, &u));
  PetscCall(VecRestoreArray(R, &r));
  PetscCall(VecViewFromOptions(R, NULL, "-residual_view"));
  PetscFunctionReturn(0);
}

static PetscErrorCode ComputeIntGradS(TS ts)
{
  PetscInt       p, Np, dim;
  PetscReal     *gradS, *velocity, *weights;
  DM             sw;
  Vec            sol;
  AppCtx        *user;
  
  PetscFunctionBeginUser;
  PetscCall(TSGetDM(ts, &sw));
  PetscCall(DMGetApplicationContext(sw, &user));
  PetscCall(TSGetSolution(ts, &sol));
  PetscCall(DMGetDimension(sw, &dim));
  PetscCall(VecGetLocalSize(sol, &Np));
  Np /= dim;
  PetscCall(DMSwarmGetField(sw, "gradS", NULL, NULL, (void **) &gradS));
  PetscCall(DMSwarmGetField(sw, "velocity", NULL, NULL, (void **) &velocity));
  PetscCall(DMSwarmGetField(sw, "w_q", NULL, NULL, (void **) &weights));
  for (p = 0; p < Np; ++p) {
    //PetscPrintf(PETSC_COMM_WORLD, "GradS part %i\n", p);
    //PetscCall(ComputeGradS(dim, weights, Np, &velocity[p*dim], velocity, &gradS[p*dim], p, user));
    PetscCall(ComputeGradSExperimental(dim, weights, Np, &velocity[p*dim], velocity, &gradS[p*dim], p, user));
    //PetscPrintf(PETSC_COMM_WORLD, "GradS part %i\n x:%g y:%g z:%g\n", p, gradS[p*dim], gradS[p*dim+1], gradS[p*dim+2]);
  }
  PetscCall(DMSwarmRestoreField(sw, "w_q", NULL, NULL, (void **) &weights));
  PetscCall(DMSwarmRestoreField(sw, "velocity", NULL, NULL, (void **) &velocity));
  PetscCall(DMSwarmRestoreField(sw, "gradS", NULL, NULL, (void **) &gradS));

  PetscFunctionReturn(0);
}

static PetscErrorCode TestDistribution(DM sw, PetscReal confidenceLevel, AppCtx *user)
{
  Vec            locv, locsv;
  PetscProbFunc  cdf;
  PetscReal      alpha;
  PetscScalar   *a;
  PetscReal     *velocity;
  PetscInt      *sn, *species;
  PetscInt       dim, d, n, p, Ns, s, off;
  MPI_Comm       comm;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscObjectGetComm((PetscObject) sw, &comm);CHKERRQ(ierr);
  ierr = DMGetDimension(sw, &dim);CHKERRQ(ierr);
  switch (dim) {
    case 1: cdf = PetscCDFMaxwellBoltzmann1D;break;
    case 2: cdf = PetscCDFMaxwellBoltzmann2D;break;
    case 3: cdf = PetscCDFMaxwellBoltzmann3D;break;
    SETERRQ(PETSC_COMM_SELF, PETSC_ERR_ARG_OUTOFRANGE, "Do not support dimension%" PetscInt_FMT, dim);
  }
  PetscCall(DMSwarmGetNumSpecies(sw, &Ns));
  PetscCall(DMSwarmGetLocalSize(sw, &n));
  if (Ns <= 1) {
    PetscCall(DMSwarmCreateLocalVectorFromField(sw, "velocity", &locv));
    PetscCall(PetscProbComputeKSStatistic(locv, cdf, &alpha));
    PetscCall(DMSwarmDestroyLocalVectorFromField(sw, "velocity", &locv));
    if (alpha < confidenceLevel) PetscCall(PetscPrintf(comm, "The KS test accepts the null hypothesis at level %.2g\n", (double) confidenceLevel));
    else                         PetscCall(PetscPrintf(comm, "The KS test rejects the null hypothesis at level %.2g (%.2g)\n", (double) confidenceLevel, (double) alpha));
  } else {
    PetscCall(PetscCalloc1(Ns, &sn));
    PetscCall(DMSwarmGetField(sw, "velocity", NULL, NULL, (void **) &velocity));
    PetscCall(DMSwarmGetField(sw, "species", NULL, NULL, (void **) &species));
    for (p = 0; p < n; ++p) ++sn[species[p]];
    for (s = 0; s < Ns; ++s) {
      PetscCall(VecCreateSeq(PETSC_COMM_SELF, sn[s]*dim, &locsv));
      PetscCall(VecSetBlockSize(locsv, dim));
      PetscCall(VecGetArray(locsv, &a));
      for (p = 0, off = 0; p < n; ++p) {
        if (species[p] == s) for (d = 0; d < dim; ++d) a[off++] = (user->v0[0]/user->v0[s]) * velocity[p*dim+d];
      }
      PetscCall(VecRestoreArray(locsv, &a));
      PetscCall(PetscProbComputeKSStatistic(locsv, cdf, &alpha));
      PetscCall(VecDestroy(&locsv));
      if (alpha < confidenceLevel) PetscCall(PetscPrintf(comm, "The KS test accepts the null hypothesis for species %" PetscInt_FMT " at level %.2g\n", s, (double) confidenceLevel));
      else                         PetscCall(PetscPrintf(comm, "The KS test rejects the null hypothesis for species %" PetscInt_FMT " at level %.2g (%.2g)\n", s, (double) confidenceLevel, (double) alpha));
    }
    PetscCall(DMSwarmRestoreField(sw, "velocity", NULL, NULL, (void **) &velocity));
    PetscCall(DMSwarmRestoreField(sw, "species", NULL, NULL, (void **) &species));
    PetscCall(PetscFree(sn));
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode CalculateMomentsAndTemperatures_Anisotropic(DM sw, PetscReal* momentum, PetscReal *KE, PetscReal* T)
{
  AppCtx        *user;
  PetscInt       Np, p, dim, d, cStart, cEnd, s;
  PetscInt      *species, Ns;
  PetscReal     *velocities, *weights, normalization;
  DM             plex;
  
  PetscFunctionBegin;
  PetscCall(DMSwarmGetCellDM(sw, &plex));
  PetscCall(DMSwarmGetNumSpecies(sw, &Ns));
  PetscCall(DMGetApplicationContext(sw, (void **) &user));
  PetscCall(DMGetDimension(plex, &dim));
  PetscCall(DMPlexGetHeightStratum(plex, 0, &cStart, &cEnd));
  PetscCall(DMSwarmSortGetAccess(sw));
  PetscCall(DMSwarmSortGetNumberOfPointsPerCell(sw, cStart, &Np));
  PetscCall(DMSwarmSortRestoreAccess(sw));
  PetscCall(DMSwarmGetField(sw, "velocity", NULL, NULL, (void **) &velocities));
  PetscCall(DMSwarmGetField(sw, "w_q", NULL, NULL, (void **) &weights));
  PetscCall(DMSwarmGetField(sw, "species", NULL, NULL, (void **) &species));
  /* Weights assumed to be 1/Np/s = 2/Np, weight needs correcting in swarm initialization */
  for (p = 0; p < Np; ++p){
    PetscReal v2[3] = {0., 0., 0.};

    normalization = user->v0[0]/user->v0[species[p]];
    for (d=0; d < dim; ++d) momentum[species[p]*dim+d] += velocities[p*dim+d] * user->v0[species[p]*dim+d] * normalization * 2./Np;
    for (d=0; d < dim; ++d) v2[d] += PetscSqr(velocities[p*dim+d]);
    for (d=0; d < dim; ++d) KE[species[p]*dim+d] += 0.5*PetscSqr(normalization* user->v0[species[p]*dim+d])*user->masses[species[p]]*2./Np*v2[d];

  }
  for (s=0; s < Ns; ++s){

    for (d=0; d < dim; ++d) T[s*dim+d] = KE[s*dim+d];
    for (d=0; d < dim; ++d) T[s*dim+d] -= PetscSqr(momentum[species[s]*dim + d]/user->n0[s]);
    for (d=0; d < dim; ++d) T[s*dim+d] *= KEV_J;

  }
  PetscCall(DMSwarmRestoreField(sw, "velocity", NULL, NULL, (void **) &velocities));
  PetscCall(DMSwarmRestoreField(sw, "w_q", NULL, NULL, (void **) &weights));
  PetscCall(DMSwarmRestoreField(sw, "species", NULL, NULL, (void **) &species));
  PetscFunctionReturn(0);
}

static PetscErrorCode CalculateMomentsAndTemperatures(DM sw, PetscReal* momentum, PetscReal *KE, PetscReal* T)
{
  AppCtx        *user;
  PetscInt       Np, p, dim, d, cStart, cEnd, s;
  PetscInt      *species, Ns;
  PetscReal     *velocities, *weights, normalization;
  DM             plex;
  
  PetscFunctionBegin;
  PetscCall(DMSwarmGetCellDM(sw, &plex));
  PetscCall(DMSwarmGetNumSpecies(sw, &Ns));
  PetscCall(DMGetApplicationContext(sw, (void **) &user));
  PetscCall(DMGetDimension(plex, &dim));
  PetscCall(DMPlexGetHeightStratum(plex, 0, &cStart, &cEnd));
  PetscCall(DMSwarmSortGetAccess(sw));
  PetscCall(DMSwarmSortGetNumberOfPointsPerCell(sw, cStart, &Np));
  PetscCall(DMSwarmSortRestoreAccess(sw));
  PetscCall(DMSwarmGetField(sw, "velocity", NULL, NULL, (void **) &velocities));
  PetscCall(DMSwarmGetField(sw, "w_q", NULL, NULL, (void **) &weights));
  PetscCall(DMSwarmGetField(sw, "species", NULL, NULL, (void **) &species));
  /* Weights assumed to be 1/Np/s = 2/Np, weight needs correcting in swarm initialization */
  for (p = 0; p < Np; ++p){
    PetscReal v2 = 0.;

    normalization = user->v0[0]/user->v0[species[p]];
    for (d=0; d < dim; ++d) momentum[species[p]*dim+d] += velocities[p*dim+d] * user->v0[species[p]] * normalization * 2./Np;
    for (d=0; d < dim; ++d) v2 += PetscSqr(velocities[p*dim+d]);
    KE[species[p]] += 0.5*PetscSqr(normalization* user->v0[species[p]])*user->masses[species[p]]*2./Np*v2;

  }
  for (s=0; s < Ns; ++s){

    T[s] = KE[s];
    for (d=0; d < dim; ++d) T[s] -= PetscSqr(momentum[species[s]*dim + d]/user->n0[s]);
    T[s] *= KEV_J;

  }
  PetscCall(DMSwarmRestoreField(sw, "velocity", NULL, NULL, (void **) &velocities));
  PetscCall(DMSwarmRestoreField(sw, "w_q", NULL, NULL, (void **) &weights));
  PetscCall(DMSwarmRestoreField(sw, "species", NULL, NULL, (void **) &species));
  PetscFunctionReturn(0);
}

static PetscErrorCode Monitor(TS ts)
{
  AppCtx    *user;
  DM         sw;
  PetscReal *T, *KE, *mom;
  PetscInt   s, Ns, dim;

  PetscFunctionBeginUser;
  PetscCall(TSGetDM(ts, &sw));
  PetscCall(DMGetApplicationContext(sw, (void **) &user));
  PetscCall(DMSwarmGetNumSpecies(sw, &Ns));
  PetscCall(DMGetDimension(sw, &dim));
  
  if (user->anisotropic) {
    PetscCall(PetscCalloc3(Ns*dim, &T, Ns*dim, &KE, dim*Ns, &mom));
    PetscCall(CalculateMomentsAndTemperatures_Anisotropic(sw, mom, KE, T));
  }
  else{
    PetscCall(PetscCalloc3(Ns, &T, Ns, &KE, dim*Ns, &mom));
    PetscCall(CalculateMomentsAndTemperatures(sw, mom, KE, T));
  } 
  for (s = 0; s < Ns; ++s ){
    if (user->anisotropic) PetscCall(PetscPrintf(PETSC_COMM_WORLD, "momentum[%i]: %g KEx[%i]: %g KEy[%i]: %g KEz[%i]: %g Tx[%i]: %g Ty[%i]: %g Tz[%i]: %g\n", s, mom[s], s, KE[s*dim+0], s, KE[s*dim+1], s, KE[s*dim+2], s, T[s*dim + 0], s, T[s*dim + 1], s, T[s*dim + 2]));
    else PetscCall(PetscPrintf(PETSC_COMM_WORLD, "momentum[%i]: %g KE[%i]: %g T[%i]: %g\n", s, mom[s], s, KE[s], s, T[s]));
  }
  if (!user->anisotropic) PetscCall(TestDistribution(sw, 0.05, user));
  PetscCall(PetscFree3(T, KE, mom));
  PetscFunctionReturn(0);
}

/*
 TS Post Step Function. Copy the solution back into the swarm for migration. We may also need to reform
 the solution vector in cases of particle migration, but we forgo that here since there is no velocity space grid
 to migrate between.
*/
static PetscErrorCode UpdateSwarm(TS ts)
{
  PetscInt idx, n;
  const PetscScalar *u;
  PetscScalar *velocity;
  DM sw;
  Vec sol;

  PetscFunctionBeginUser;
  PetscCall(TSGetDM(ts, &sw));
  PetscCall(DMSwarmGetField(sw, "velocity", NULL, NULL, (void **) &velocity));
  PetscCall(TSGetSolution(ts, &sol));
  PetscCall(VecGetArrayRead(sol, &u));
  PetscCall(VecGetLocalSize(sol, &n));
  for (idx = 0; idx < n; ++idx) velocity[idx] = u[idx];
  PetscCall(VecRestoreArrayRead(sol, &u));
  PetscCall(DMSwarmRestoreField(sw, "velocity", NULL, NULL, (void **) &velocity));
  PetscCall(Monitor(ts));
  PetscFunctionReturn(0);
}

static PetscErrorCode InitializeSolve(TS ts, Vec u)
{
  PetscInt       Np;
  DM             sw, plex;
  Vec            v;
  AppCtx        *user;
  
  PetscFunctionBeginUser;
  PetscCall(TSGetDM(ts, &sw));
  PetscCall(DMGetApplicationContext(sw, (void **) &user));
  PetscCall(DMSwarmGetCellDM(sw, &plex));
  PetscCall(DMSwarmCreateGlobalVectorFromField(sw, "velocity", &v));
  PetscCall(VecCopy(v, u));
  PetscCall(DMSwarmDestroyGlobalVectorFromField(sw, "velocity", &v));
  PetscCall(VecViewFromOptions(u, NULL, "-ic_view"));
  PetscCall(VecGetLocalSize(u, &Np));
  PetscCall(PetscPrintf(PETSC_COMM_WORLD, "Np: %i\n", Np/3));
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  TS             ts;     /* nonlinear solver */
  DM             dm, sw; /* Velocity space mesh and Particle Swarm */
  Vec            u, v;   /* problem vector */
  MPI_Comm       comm;
  AppCtx         user;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;
  PetscCall(ProcessOptions(comm, &user));
  /* Initialize objects and set initial conditions */

  
  PetscCall(CreateMesh(comm, &dm, &user));
  PetscCall(CreateSwarm(dm, &user, &sw));
  PetscCall(DMSetApplicationContext(sw, &user));

  /* Temporary calls to tabulate quadratur  */
  PetscInt cdim;
  int dim, ncp;

  PetscCall(DMGetDimension(dm, &cdim));
  dim = (int)cdim;
  double *points, *qw;

  double epsi = PetscSqrtReal(-2*user.epsilon*PetscLogReal(1000.*PETSC_MACHINE_EPSILON/(2*user.epsilon)));
  PetscCall(GenerateSphericalQuadrature(dim, epsi, &points, &qw, &ncp));
  PetscCall(PetscEnd());
  
  PetscCall(DMSwarmVectorDefineField(sw, "velocity"));
  PetscCall(TSCreate(comm, &ts));
  PetscCall(TSSetDM(ts, sw));
  PetscCall(TSSetMaxTime(ts, 100.0));
  PetscCall(TSSetTimeStep(ts, user.step_size));
  PetscCall(TSSetMaxSteps(ts, user.steps));
  PetscCall(TSSetExactFinalTime(ts, TS_EXACTFINALTIME_MATCHSTEP));
  PetscCall(TSSetRHSFunction(ts, NULL, RHSFunctionParticles, &user));
  PetscCall(TSSetFromOptions(ts));
  PetscCall(TSSetComputeInitialCondition(ts, InitializeSolve));
  PetscCall(DMSwarmCreateGlobalVectorFromField(sw, "velocity", &v));
  PetscCall(VecDuplicate(v, &u));
  PetscCall(DMSwarmDestroyGlobalVectorFromField(sw, "velocity", &v));
  PetscCall(TSComputeInitialCondition(ts, u));
  //PetscEnd();
  PetscCall(VecViewFromOptions(u, NULL, "-ic_view"));
  PetscCall(TSSetPreStep(ts, ComputeIntGradS));
  PetscCall(TSSetPostStep(ts, UpdateSwarm));
  /* Test the initial distribution. */
  if (!user.anisotropic) PetscCall(TestDistribution(sw, 0.05, &user));
  PetscCall(TSSolve(ts, u));
  PetscCall(VecDestroy(&u));
  PetscCall(TSDestroy(&ts));
  PetscCall(DMDestroy(&sw));
  PetscCall(DMDestroy(&dm));
  PetscCall(PetscFinalize());
  return 0;
}

/*TEST
  test:
    suffix: 2d_one_species
    requires: ks triangle !single !complex
    args: -steps 1 -step_size 0.01\
    ts_type theta -ts_theta_theta 0.5\
    -dm_plex_simplex 0 -dm_plex_dim 2\
    -dm_plex_box_lower -1,-1\
    -dm_plex_box_upper 1,1\
    -dm_plex_box_faces 1,1\
    -dm_swarm_num_particles 20\
    -dm_swarm_coordinate_density gaussian\
    -snes_monitor\
    -snes_mf\
    -snes_monitor\
    -dm_swarm_num_species 2\
    -dm_swarm_masses 1.,1.\
    -dm_swarm_mass_units 0,0\
    -dm_swarm_charges -1,-1\
    -dm_swarm_temperature 5,6
  test:
    suffix: 2d_two_species
    requires: ks triangle !single !complex
    args: -steps 1 -step_size 0.01\
    ts_type theta -ts_theta_theta 0.5\
    -dm_plex_simplex 0 -dm_plex_dim 2\
    -dm_plex_box_lower -1,-1\
    -dm_plex_box_upper 1,1\
    -dm_plex_box_faces 1,1\
    -dm_swarm_num_particles 20\
    -dm_swarm_coordinate_density gaussian\
    -snes_monitor\
    -snes_mf\
    -snes_monitor\
    -dm_swarm_num_species 2\
    -dm_swarm_masses 1.,2.\
    -dm_swarm_mass_units 0,1\
    -dm_swarm_charges -1,1\
    -dm_swarm_temperature 5,6
  test:
    suffix: 3d_one_species
    requires: ks triangle !single !complex
    args: -steps 1 -step_size 0.01\
    -ts_type theta -ts_theta_theta 0.5\
    -dm_plex_simplex 0 -dm_plex_dim 3\
    -dm_plex_box_lower -1,-1,-1\
    -dm_plex_box_upper 1,1,1\
    -dm_plex_box_faces 1,1,1\
    -dm_swarm_num_particles 20\
    -dm_swarm_coordinate_density gaussian\
    -snes_monitor\
    -snes_mf\
    -snes_monitor\
    -dm_swarm_num_species 2\
    -dm_swarm_masses 1.,1.\
    -dm_swarm_mass_units 0,0\
    -dm_swarm_charges -1,-1\
    -dm_swarm_temperature 5,6
  test:
  suffix: 3d_two_species
    requires: ks triangle !single !complex
    args: -steps 1 -step_size 0.01\
    -ts_type theta -ts_theta_theta 0.5\
    -dm_plex_simplex 0 -dm_plex_dim 3\
    -dm_plex_box_lower -1,-1,-1\
    -dm_plex_box_upper 1,1,1\
    -dm_plex_box_faces 1,1,1\
    -dm_swarm_num_particles 20\
    -dm_swarm_coordinate_density gaussian\
    -snes_monitor\
    -snes_mf\
    -snes_monitor\
    -dm_swarm_num_species 2\
    -dm_swarm_masses 1.,2.\
    -dm_swarm_mass_units 0,1\
    -dm_swarm_charges -1,1\
    -dm_swarm_temperature 5,6
TEST*/
