algoim: algoim.cpp
	clang++ -march=znver4 -Wall -O3 -I/home/rylanor/amd-libflame/include/LP64/ -L/home/rylanor/amd-libflame/lib/LP64/ -lflame -Wl,-rpath=/home/rylanor/amd-libflame/lib/LP64/ -I/home/rylanor/amd-blis/include/LP64 -L/home/rylanor/amd-blis/lib/LP64/ -lblis -Wl,-rpath=/home/rylanor/amd-blis/lib/LP64/ -shared -fPIC -std=c++17 algoim.cpp -o libalgoim.so

ex27: ex27.o 
	${CLINKER} -o $@ $< -L. ${PETSC_LIB} ${PWD}/algoim.so

include ${PETSC_DIR}/lib/petsc/conf/rules
include ${PETSC_DIR}/lib/petsc/conf/variables