import matplotlib.pyplot as plt
import numpy as np
import argparse

parser = argparse.ArgumentParser(description="test set")
parser.add_argument('--simplex', action='store_true')
args = parser.parse_args()

steps_10 = []
steps_100 = []
steps_1000 = []
temps_10_1 = []
temps_10_2 = []
temps_100_1 = []
temps_100_2 = []
temps_1000_1 = []
temps_1000_2 = []
files = ["output/landau_two_ten.out", "output/landau_two_100.out", "output/landau_two_1000.out"]
files_dt01 = ["temp/output/landau_two_10_dt01.out", "temp/output/landau_two_100_dt01.out", "temp/output/landau_two_1000_dt01.out"]
files_dt1 = ["temp/output/landau_two_10_dt1.out", "temp/output/landau_two_100_dt1.out", "temp/output/landau_two_1000_dt1.out"]

files_op = ["op_land/op_landau_two_10.out", "op_land/op_landau_two_100.out", "op_land/op_landau_two_1000.out"]

files_test = ["output2.out"]

files = files_test

for f in files:
    count = 0
    file = open(f, 'r')
    lines = file.readlines()
    tidx = files.index(f)
    if tidx == 0:
        temps_1  = temps_10_1
        temps_2  = temps_10_2
        steps = steps_10
    elif tidx == 1:
        temps_1 = temps_100_1
        temps_2 = temps_100_2
        steps = steps_100
    else:
        temps_1 = temps_1000_1
        temps_2 = temps_1000_2
        steps =  steps_1000
    for line in lines:
        if line.find("T[0]") != -1:
            temps_1.append(float(line.split()[5]))
        elif line.find("T[1]") != -1:
            temps_2.append(float(line.split()[5]))
            steps.append(count)
            count = count + 1
        else:
            continue


plt.plot(steps_10, temps_10_1, steps_10, temps_10_2)
plt.legend(['population 1', 'Population 2'])
plt.title("Thermal Population Temperatures (N_ps = 432, dt=0.01)")
plt.ylabel("Temperature")
plt.xlabel("Step (dt = 0.01)")    
#plt.show()
plt.savefig("plandau_1species2pop3D.png")
plt.clf()

plt.plot(steps_100, temps_100_1, steps_100, temps_100_2)
plt.legend(['100N 1', '100N 2'])
plt.title("Thermal Population Temperatures (N_ps = 100, dt=0.001)")
plt.ylabel("Temperature")
plt.xlabel("Step (dt = 0.001)")    
#plt.show()
plt.savefig("plandau_1species2popN100.png")
plt.clf()

plt.plot(steps_1000, temps_1000_1, steps_1000, temps_1000_2)
plt.legend(['1000N 1', '1000N 2'])
plt.title("Thermal Population Temperatures (N_ps = 1000, dt=0.001)")
plt.ylabel("Temperature")
plt.xlabel("Step (dt = 0.001)")    
#plt.show()
plt.savefig("plandau_1species2popN1000.png")
plt.clf()
'''
steps_10.clear()
steps_100.clear()
steps_1000.clear()
temps_10_1.clear()
temps_10_2.clear()
temps_100_1.clear()
temps_100_2.clear()
temps_1000_1.clear()
temps_1000_2.clear()


for f in files_dt01:
    count = 0
    file = open(f, 'r')
    lines = file.readlines()
    tidx = files_dt01.index(f)
    if tidx == 0:
        temps_1  = temps_10_1
        temps_2  = temps_10_2
        steps = steps_10
    elif tidx == 1:
        temps_1 = temps_100_1
        temps_2 = temps_100_2
        steps = steps_100
    else:
        temps_1 = temps_1000_1
        temps_2 = temps_1000_2
        steps =  steps_1000
    for line in lines:
        if line.find("T[0]") != -1:
            temps_1.append(float(line.split()[5]))
        elif line.find("T[1]") != -1:
            temps_2.append(float(line.split()[5]))
            steps.append(count)
            count = count + 1
        else:
            continue


plt.plot(steps_10, temps_10_1, steps_10, temps_10_2)
plt.legend(['10N 1', '10N 2'])
plt.title("Thermal Population Temperatures (N_ps = 10, dt=0.01)")
plt.ylabel("Temperature")
plt.xlabel("Step (dt = 0.01)")    
#plt.show()
plt.savefig("plandau_1species2popN10dt01.png")
plt.clf()

plt.plot(steps_100, temps_100_1, steps_100, temps_100_2)
plt.legend(['100N 1', '100N 2'])
plt.title("Thermal Population Temperatures (N_ps = 100, dt=0.01)")
plt.ylabel("Temperature")
plt.xlabel("Step (dt = 0.01)")    
#plt.show()
plt.savefig("plandau_1species2popN100dt01.png")
plt.clf()

plt.plot(steps_1000, temps_1000_1, steps_1000, temps_1000_2)
plt.legend(['1000N 1', '1000N 2'])
plt.title("Thermal Population Temperatures (N_ps = 1000, dt=0.01)")
plt.ylabel("Temperature")
plt.xlabel("Step (dt = 0.01)")    
#plt.show()
plt.savefig("plandau_1species2popN1000dt01.png")
plt.clf()

steps_10.clear()
steps_100.clear()
steps_1000.clear()
temps_10_1.clear()
temps_10_2.clear()
temps_100_1.clear()
temps_100_2.clear()
temps_1000_1.clear()
temps_1000_2.clear()



for f in files_dt1:
    count = 0
    file = open(f, 'r')
    lines = file.readlines()
    tidx = files_dt1.index(f)
    if tidx == 0:
        temps_1  = temps_10_1
        temps_2  = temps_10_2
        steps = steps_10
    elif tidx == 1:
        temps_1 = temps_100_1
        temps_2 = temps_100_2
        steps = steps_100
    else:
        temps_1 = temps_1000_1
        temps_2 = temps_1000_2
        steps =  steps_1000
    for line in lines:
        if line.find("T[0]") != -1:
            temps_1.append(float(line.split()[5]))
        elif line.find("T[1]") != -1:
            temps_2.append(float(line.split()[5]))
            steps.append(count)
            count = count + 1
        else:
            continue


plt.plot(steps_10, temps_10_1, steps_10, temps_10_2)
plt.legend(['10N 1', '10N 2'])
plt.title("Thermal Population Temperatures (N_ps = 10, dt=0.1)")
plt.ylabel("Temperature")
plt.xlabel("Step (dt = 0.1)")    
#plt.show()
plt.savefig("plandau_1species2popN10dt1.png")
plt.clf()

plt.plot(steps_100, temps_100_1, steps_100, temps_100_2)
plt.legend(['Population 1', 'Population 2'])
plt.title("Thermal Population Temperatures (N_ps = 100, dt=0.1)")
plt.ylabel("Temperature")
plt.xlabel("Step (dt = 0.1)")    
#plt.show()
plt.savefig("plandau_1species2popN100dt1.png")
plt.clf()

plt.plot(steps_1000, temps_1000_1, steps_1000, temps_1000_2)
plt.legend(['1000N 1', '1000N 2'])
plt.title("Thermal Population Temperatures (N_ps = 1000, dt=0.1)")
plt.ylabel("Temperature")
plt.xlabel("Step (dt = 0.1)")    
#plt.show()
plt.savefig("plandau_1species2popN1000dt1.png")
plt.clf()


steps_10.clear()
steps_100.clear()
steps_1000.clear()
temps_10_1.clear()
temps_10_2.clear()
temps_100_1.clear()
temps_100_2.clear()
temps_1000_1.clear()
temps_1000_2.clear()



for f in files_op:
    count = 0
    file = open(f, 'r')
    lines = file.readlines()
    tidx = files_op.index(f)
    if tidx == 0:
        temps_1  = temps_10_1
        temps_2  = temps_10_2
        steps = steps_10
    elif tidx == 1:
        temps_1 = temps_100_1
        temps_2 = temps_100_2
        steps = steps_100
    else:
        temps_1 = temps_1000_1
        temps_2 = temps_1000_2
        steps =  steps_1000
    for line in lines:
        if line.find("T[0]") != -1:
            temps_1.append(float(line.split()[5]))
        elif line.find("T[1]") != -1:
            temps_2.append(float(line.split()[5]))
            steps.append(count)
            count = count + 1
        else:
            continue


plt.plot(steps_10, temps_10_1, steps_10, temps_10_2)
plt.legend(['10N 1', '10N 2'])
plt.title("REVThermal Population Temperatures (N_ps = 10 REV)")
plt.ylabel("REVTemperature")
plt.xlabel("REVStep (dt = 0.001)")    
#plt.show()
plt.savefig("REVplandau_1species2popN10dt1.png")
plt.clf()

plt.plot(steps_100, temps_100_1, steps_100, temps_100_2)
plt.legend(['Population 1', 'Population 2'])
plt.title("REVThermal Population Temperatures (N_ps = 100 REV)")
plt.ylabel("REVTemperature")
plt.xlabel("REVStep (dt = 0.001)")    
#plt.show()
plt.savefig("REVplandau_1species2popN100dt1.png")
plt.clf()

plt.plot(steps_1000, temps_1000_1, steps_1000, temps_1000_2)
plt.legend(['1000N 1', '1000N 2'])
plt.title("REV Thermal Population Temperatures (N_ps = 1000 REV)")
plt.ylabel("REV Temperature")
plt.xlabel("REV Step (dt = 0.001)")    
#plt.show()
plt.savefig("REVplandau_1species2popN1000dt1.png")
plt.clf()
'''